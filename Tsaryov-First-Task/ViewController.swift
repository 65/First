//
//  ViewController.swift
//  Tsaryov-First-Task
//
//  Created by Ivan Tsaryov on 01/01/2018.
//  Copyright © 2018 Ivan Tsaryov. All rights reserved.
//

import UIKit

protocol Loadable {
    
    var task: URLSessionDataTask? { get set }
}

class ViewController: UIViewController {
    
    let cellIdentifier = "ImageTableViewCell"
    
    @IBOutlet weak var tableView: UITableView!
    
    private let session = URLSession.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let cellNib = UINib(nibName: cellIdentifier, bundle: Bundle.main)
        tableView.register(cellNib, forCellReuseIdentifier: cellIdentifier)
        
        tableView.dataSource = self
    }
    
    func downloadImage(withURL url: URL, forCell cell: UITableViewCell) {
        // Use any cache setting
        let request = URLRequest(url: url, cachePolicy: .returnCacheDataElseLoad)
        
        let task = session.dataTask(with: request) { (data, _, _) in
            guard let data = data else {
                return
            }
            
            DispatchQueue.main.async {
                cell.imageView?.image = UIImage(data: data)
                cell.setNeedsLayout()
            }
        }
        
        if var cell = cell as? Loadable {
            cell.task?.cancel()
            cell.task = task
        }
        
        task.resume()
    }
}

extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        let url = "http://placehold.it/375x150?text=\(indexPath.row)"

        if let imageURL = URL(string: url) {
            downloadImage(withURL: imageURL, forCell: cell)
        }

        return cell
    }
}
