//
//  ImageTableViewCell.swift
//  Tsaryov-First-Task
//
//  Created by Ivan Tsaryov on 16/01/2018.
//  Copyright © 2018 Ivan Tsaryov. All rights reserved.
//

import UIKit

class ImageTableViewCell: UITableViewCell, Loadable {
    
    @IBOutlet weak var imgView: UIImageView!
    
    var task: URLSessionDataTask?
    
    override var imageView: UIImageView? {
        return imgView
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        imgView.image = nil
    }
}
